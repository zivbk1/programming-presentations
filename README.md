# Programming Presentations

How to make beautiful presentations with Svelte & SvelteKit, using Reveal.js.  
https://prog-pres-2023.netlify.app/

## How to start a project like this.

1. Install SvelteKit and Svelte

`npm create svelte@latest`

* Where should we create your project? - `Enter`
* Directory not empty. Continue? - `Yes`
* Which Svelte app template? - `Skeleton project`
* Add type checking with TypeScript? - `Yes, using JavaScript with JSDoc comments`
* Select additional options (use arrow keys/space bar) - `Add Prettier for code formatting`

1. Make sure all packages are installed.

`npm i`

1. Add TailwindCSS for easy component styling.

`npx svelte-add tailwindcss`

1. Add Reveal.js and a couple of custom fonts.

`npm i reveal.js @types/reveal.js @fontsource/manrope @fontsource/jetbrains-mono`

1. Start the SvelteKit development server.

`npm run dev`

1. Profit!!!
